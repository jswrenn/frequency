The [`Frequency`][frequency] trait represents types that keep track of the 
observed counts of items. The [`BTreeMapFrequency`][btreemapfrequency] type
is a frequency counter backed by a [`BTreeMap`][btreemap].

# Usage
Add [`frequency`][freq_crate] and `frequency-btreemap` to your `Cargo.toml`:

```
[dependencies]
frequency = "^1.0.0"
frequency-btreemap = "^1.0.0"
```

Import both [`Frequency`][frequency] and 
[`BTreeMapFrequency`][btreemapfrequency]:

```
extern crate frequency;
extern crate frequency_btreemap;

use frequency::Frequency;
use frequency_btreemap::BTreeMapFrequency;
```

[freq_crate]: https://docs.rs/frequency/~1/
[frequency]: https://docs.rs/frequency/~1/frequency/trait.Frequency.html
[btreemapfrequency]: struct.BTreeMapFrequency.html
[btreemap]: https://doc.rust-lang.org/std/collections/struct.BTreeMap.html