The [`Frequency`][frequency] trait represents types that keep track of the 
observed counts of items. The [`OrderMapFrequency`][ordermapfrequency] type
is a frequency counter backed by a [`OrderMap`][ordermap].

# Usage
Add [`frequency`][freq_crate] and `frequency-ordermap` to your `Cargo.toml`:

```
[dependencies]
frequency = "^1.0.0"
frequency-ordermap = "^1.0.0"
```

Import both [`Frequency`][frequency] and 
[`OrderMapFrequency`][ordermapfrequency]:

```
extern crate frequency;
extern crate frequency_ordermap;

use frequency::Frequency;
use frequency_ordermap::OrderMapFrequency;
```

[freq_crate]: https://docs.rs/frequency/~1/
[frequency]: https://docs.rs/frequency/~1/frequency/trait.Frequency.html
[ordermapfrequency]: struct.OrderMapFrequency.html
[ordermap]: https://docs.rs/ordermap/0.2.8/ordermap/struct.OrderMap.html