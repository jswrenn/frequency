The [`Frequency`][frequency] trait represents types that keep track of the 
observed counts of items. The [`HashMapFrequency`][hashmapfrequency] type
is a frequency counter backed by a [`HashMap`][hashmap].

# Usage
Add [`frequency`][freq_crate] and `frequency-hashmap` to your `Cargo.toml`:

```
[dependencies]
frequency = "^1.0.0"
frequency-hashmap = "^1.0.0"
```

Import both [`Frequency`][frequency] and 
[`HashMapFrequency`][hashmapfrequency]:

```
extern crate frequency;
extern crate frequency_hashmap;

use frequency::Frequency;
use frequency_hashmap::HashMapFrequency;
```

[freq_crate]: https://docs.rs/frequency/~1/
[frequency]: https://docs.rs/frequency/~1/frequency/trait.Frequency.html
[hashmapfrequency]: struct.HashMapFrequency.html
[hashmap]: https://doc.rust-lang.org/std/collections/struct.HashMap.html
