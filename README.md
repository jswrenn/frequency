The [`Frequency`][frequency] trait represents types that keep track of the
observed counts of items.

# Usage
Add `frequency` to your `Cargo.toml`:

```
[dependencies]
frequency = "^1.0.0"
```

To use the `Frequency` interface for types implementing `Frequency`,
you must import the `Frequency` trait:

```
extern crate frequency;

use frequency::Frequency;
```

Implementations of [`Frequency`][frequency] are provided by the
[`frequency-btreemap`][frequency_btreemap],
[`frequency-btreemap`][frequency_hashmap], and
[`frequency-btreemap`][frequency_ordermap] crates.

[frequency]: https://docs.rs/frequency/~1/frequency/trait.Frequency.html
[frequency_btreemap]: https://docs.rs/frequency-btreemap/~1/
[frequency_hashmap]: https://docs.rs/frequency-hashmap/~1/
[frequency_ordermap]: https://docs.rs/frequency-ordermap/~1/