//! The [`Frequency`][frequency] trait represents types that keep track of the
//! observed counts of items.
//!
//! # Usage
//! Add `frequency` to your `Cargo.toml`:
//!
//! ```
//! [dependencies]
//! frequency = "^1.0.0"
//! ```
//!
//! To use the `Frequency` interface for types implementing `Frequency`,
//! you must import the `Frequency` trait:
//!
//! ```
//! extern crate frequency;
//!
//! use frequency::Frequency;
//! ```
//!
//! Implementations of [`Frequency`][frequency] are provided by the
//! [`frequency-btreemap`][frequency_btreemap],
//! [`frequency-btreemap`][frequency_hashmap], and
//! [`frequency-btreemap`][frequency_ordermap] crates.
//! 
//! [frequency]: https://docs.rs/frequency/~1/frequency/trait.Frequency.html
//! [frequency_btreemap]: https://docs.rs/frequency-btreemap/~1/
//! [frequency_hashmap]: https://docs.rs/frequency-hashmap/~1/
//! [frequency_ordermap]: https://docs.rs/frequency-ordermap/~1/

extern crate num_traits;

use num_traits::Num;

pub trait Frequency<'t, T>
    where &'t Self: 't + IntoIterator,
          T: 't + Eq
{
    /// The type used to record counts.
    type N: Num;

    /// The type of an iterator over item-count pairs.
    type Iter: Iterator<Item=(&'t T, &'t Self::N)>;

    /// The type of an iterator over items.
    type Items: Iterator<Item=&'t T>;

    /// The type of an iterator over counts.
    type Counts: Iterator<Item=&'t Self::N>;

    /// Returns the count of an item.
    fn count(&self, value: &T) -> Self::N;

    /// Increments the count for an item.
    fn increment(&mut self, value: T);

    /// An iterator visiting all key-value pairs.
    /// Iterator element type is `(&'t T, &'t usize)`.
    fn iter(&'t self) -> Self::Iter;

    /// An iterator visiting all keys in arbitrary order.
    /// Iterator element type is `&'t T`.
    fn items(&'t self) -> Self::Items;

    /// An iterator visiting all counts in arbitrary order.
    /// Iterator element type is `&'t usize`.
    fn counts(&'t self) -> Self::Counts;
}